var express = require('express');
var passport = require('passport');
var authController = require('../../controller/auth');
var router = express.Router();

router.get('/', authController.testControllerMethod);

//local auth using username and password
router.post('/signup', authController.userSignUp);
router.post('/login', authController.userLogin);
router.get('/profile', isLoggedIn, authController.getUserProfile);
router.get('/logout', authController.userLogout);
//auth with facebook
router.get('/facebook', authController.facebookAuth);
router.get('/facebook/callback', authController.facebookCallback);
//auth with google
router.get('/google', authController.googleAuth);
router.get('/google/callback', authController.googleCallback);
//auth with twitter
router.get('/twitter', authController.twitterAuth);
router.get('/twitter/callback', authController.twitterCallback);

module.exports = router;

//route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}