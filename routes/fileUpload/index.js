var express = require('express');
var router = express.Router();
var multer = require('multer');
var Image = require('../../models/image');
var imagePath;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log('File Info');
        console.log(file);
        var newImage = Image();
        imagePath = file.fieldname + '-' + Date.now() + '-' + file.originalname;
        newImage.imagePath = 'images/' + imagePath;
        newImage.save((err, newSavedImage) => {
            if (err) {
                console.log('Error in Saving the Image Path');
            } else {
                console.log('Successfully saved the image path.');
                cb(null, 'public/images')
            }
        });
    },
    filename: function (req, file, cb) {
        cb(null, imagePath);
    }
});

var upload = multer({ storage: storage }).single('image');

router.post('/', upload, (req, res) => {
    res.redirect('/fileUpload');
});

router.get('/', (req, res) => {

    Image.find((err, images) => {
        if (err) {
            console.log('Error in grabbing Images: ' + err);
        } else {
            console.log('Images');
            for (let imagePath of images) {
                console.log(imagePath);
            }
            res.render('fileUpload.ejs', {
                title: 'Home',
                images: images
            });
        }
        
    });
});

module.exports = router;

//route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}