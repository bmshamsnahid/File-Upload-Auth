var express = require('express');
var router = express.Router();
var multer = require('multer');
var Image = require('../../models/image');
var imagePath;

router.get('/', (req, res) => {

    Image.find((err, images) => {
        if (err) {
            console.log('Error in grabbing Images: ' + err);
        } else {
            console.log('Images');
            for (let imagePath of images) {
                console.log(imagePath);
            }
            res.render('fileDisplay.ejs', {
                title: 'Home',
                images: images
            });
        }
        
    });
});

module.exports = router;

//route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}