var express = require('express');
var router = express.Router();

var userController = require('../../controller/user');

router.get('/test', userController.testControllerMethod);
router.get('/', userController.getAllUser);
router.get('/:id', userController.getSingleUser);

module.exports = router;